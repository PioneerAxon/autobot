#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

///
/// \brief Length of string containing Hostaddress
///
#define HOST_LEN 1024

///
/// \brief Length of string containing Port number
///
#define PORT_LEN 8

///
/// \brief Length of Read/Write buffers
///
#define BUFF_LEN 8192

///
/// \brief Length of string containing Nickname
///
#define NICK_LEN 64

///
/// \brief Length of string containing Password
///
#define PASS_LEN 64

///
/// \brief Length of string containing Channel name
///
#define CHAN_LEN 512


///
/// \brief Length of string containing Setting attribute name
///
#define ATTR_LEN 128

///
/// \brief Length of string containing Setting value
///
#define VAL_LEN 1024

///
/// \brief Settings file name
///
#define SETTINGS_FILE_NAME "autobot.conf"

///
/// \brief Setting split character
///
/// Every setting's Attribute name and Value must be saperated by this character
///
#define SPLIT_CHAR ':'

///
/// \brief Setting split string
///
/// SPLIT_CHAR represented as string
///
#define SPLIT_STR ":"


///
/// \brief Log file name
///
#define LOG_FILE_NAME "autobot.log"

///
/// \brief Default LogLevel
///
#define DEFAULT_LOG_LEVEL LOG_ALL

#endif
