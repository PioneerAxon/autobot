#ifndef __ACL_MANAGER_H__
#define __ACL_MANAGER_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <settings_manager.h>

///
/// \brief Access Control List
///

typedef struct _ACL
{
	int list_len;	///< Length of list of authorized users.
	char** list;	///< List of authorized users.
} ACL;

///
/// \brief ACL constructor
///
/// Construct ACL from Settings.
///
/// \param[in] settings Initialized settings.
///
/// \return The newly constructed ACL structure.
///
ACL* acl_new (Settings* settings);

///
/// \brief Check access level
///
/// Check user's access rights based on ACL.
///
/// \param[in] acl ACL structure
/// \param[in] name Name of user
///
/// \return non-zero for access granted.
///
int acl_check_access (ACL* acl, const char* name);

///
/// \brief ACL destructor
///
/// Destroy ACL structure.
///
/// \param[in] acl ACL structure
///
void acl_destroy (ACL* acl);

#endif
