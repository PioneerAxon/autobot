#include <bot_manager.h>

///
/// \brief Callback structure
///
typedef struct _Callback
{
	char* on_message;		///< Message, when to call the Callback function
	CallbackType type;		///< CallbackType of the Callback function scanning
	void* (*function) (Bot* bot, const char* message, const char* from ,const char* to, const char* content);	///< Function to callback
} Callback;

///
/// \brief Callback constructor
///
/// Construct new Callback structure
///
/// \param[in] on_message Message, on which to call the function
/// \param[in] function Function, which to call on a match
/// \param[in] type CallbackType of the Callback function scanning
///
/// \return Newly allocated Callback structure.
///
static Callback* callback_new (char* on_message, void* (*function) (Bot* bot, const char* full_message, const char* from, const char* to, const char* content), CallbackType type)
{
	if (!on_message || !function)
		return NULL;
	Callback* new;
	new = (Callback*) malloc (sizeof (Callback));
	assert (new != NULL);
	new->on_message = (char*) malloc (sizeof (char) * strlen (on_message));
	strcpy (new->on_message, on_message);
	new->function = function;
	new->type = type;
	return new;
}

///
/// \brief Callback destructor
///
/// Destroys Callback structure.
///
/// \param[in] callback Callback structure
///
static void callback_destroy (Callback* callback)
{
	if (!callback)
		return;
	free (callback->on_message);
	free (callback);
}

///
/// \brief Insert Callback in Bot structure
///
/// \param[in] bot Bot structure
/// \param[in] callback Callback structure
///
static void _bot_insert_callback (Bot* bot, Callback* callback)
{
	if (!bot || !callback)
		return;
	bot->callbacks = (Callback**) realloc (bot->callbacks, sizeof (Callback*) * (bot->callback_count + 1));
	bot->callbacks [bot->callback_count] = callback;
	bot->callback_count++;
}

///
/// \brief Call appropriate Callback
///
/// Calls appropriate Callback functions on incoming messages.
///
/// \param[in] bot Bot structure
/// \param[in] on_message Parsed message content
/// \param[in] full_message Full message content
/// \param[in] from Source of private message
/// \param[in] to Destination of private message
/// \param[in] content Content of private message
///
static void _bot_call_by_message (Bot* bot, const char* on_message, const char* full_message, const char* from, const char* to, const char* content)
{
	int l;
	int flag = 0;
	if (!bot || !on_message)
		return;
	for (l = 0; l < bot->callback_count; l++)
	{
		if (bot->callbacks [l]->type == CallbackType_CaseSensitive)
		{
			if (strncmp (on_message, bot->callbacks [l]->on_message, strlen (bot->callbacks [l]->on_message)) == 0)
			{
				//Don't break after calling one function.
				//There might be more than one function registered for a single message.
				bot->callbacks [l]->function (bot, full_message, from, to, content);
				flag++;
			}
		}
		else if (bot->callbacks [l]->type == CallbackType_CaseInsensitive)
		{
			if (strncasecmp (on_message, bot->callbacks [l]->on_message, strlen (bot->callbacks [l]->on_message)) == 0)
			{
				//Don't break after calling one function.
				//There might be more than one function registered for a single message.
				bot->callbacks [l]->function (bot, full_message, from, to, content);
				flag++;
			}
		}
	}
	if (!flag && bot->default_callback)
		bot->default_callback (bot, full_message, from, to, content);
}

///
/// \brief Process incoming message
///
/// Process and call Callbacks on incoming message from IRC network.
///
/// \param[in] bot Bot structure
/// \param[in] message Received message
///
static void _bot_on_message (Bot* bot, const char* message)
{
	char from [NICK_LEN];
	char to [NICK_LEN];
	char content [BUFF_LEN];
	if (!bot || !message)
		return;
	if (message [0] != ':')
	{
		logger_log (logger, LOG_ALERT, "Bot: _bot_on_message: discarding unknown message \"%s\"", message);
		return;
	}
	if (!message_try_parse_PRIVMSG (message, from, to, content))
	{
		if (message_consume (bot, message) == MessageType_Unknown)
		{
			logger_log (logger, LOG_ALERT, "Bot: _bot_on_message: discarding unknown message \"%s\"", message);
		}
		return;
	}
	//Call message only when intended for bot.
	if (strcmp (to, bot->irc->nick) == 0)
		_bot_call_by_message (bot, content, message, from, to, content);
}


Bot* bot_new (char* host, char* port, char* nick, char* password, char* channel)
{
	Bot* new;
	new = (Bot*) malloc (sizeof (Bot));
	assert (new != NULL);
	new->irc = irc_new (host, port, nick, password, channel);
	new->callbacks = NULL;
	new->callback_count = 0;
	assert (irc_connect (new->irc) == IRCState_Connected);
	new->allow_in_main_loop = 1;
	new->default_callback = NULL;
	return new;
}

void bot_destroy (Bot* bot)
{
	if (!bot)
		return;
	irc_destroy (bot->irc);
	while (bot->callback_count--)
	{
		callback_destroy (bot->callbacks [bot->callback_count]);
	}
	free (bot->callbacks);
	free (bot);
}

void bot_insert_callback (Bot* bot, char* on_message, void* (*function) (Bot* bot, const char* full_message, const char* from, const char* to, const char* content), CallbackType type)
{
	_bot_insert_callback (bot, callback_new (on_message, function, type));
}

void bot_insert_default_callback (Bot* bot, void* (*function) (Bot* bot, const char* full_message, const char* from, const char* to, const char* content))
{
	if (bot && function)
		bot->default_callback = function;
}

void bot_do_events (Bot* bot)
{
	char buffer [BUFF_LEN];
	int ret;
	if (!bot)
		return;
	ret = irc_read_async (bot->irc, buffer, BUFF_LEN);
	if (ret == -2)
		return;
	//FIXME: Possible connection reset! Try to reconnect??
	else if (ret == 0)
		return;
	else if (ret > 0)
		_bot_on_message (bot, buffer);
	//TODO: Error in read (); Handle errors.
	else
		return;
}

void bot_main_loop (Bot* bot)
{
	char buffer [BUFF_LEN];
	int ret;
	if (!bot)
		return;
	bot->allow_in_main_loop = 1;
	while (bot->allow_in_main_loop)
	{
		ret = irc_read (bot->irc, buffer, BUFF_LEN);
		//FIXME: Possible connection reset! Try to reconnect??
		if (ret == 0)
			return;
		else if (ret > 0)
			_bot_on_message (bot, buffer);
		//TODO: Error in read (); Handle errors.
		else
			return;
	}
}

void bot_quit_main_loop (Bot* bot)
{
	if (!bot)
		return;
	bot->allow_in_main_loop = 0;
}
