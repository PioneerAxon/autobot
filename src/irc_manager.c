#include <irc_manager.h>

IRC* irc_new (char* host, char* port, char* nick, char* password, char* channel)
{
	IRC* new;
	new = (IRC*) malloc (sizeof (IRC));
	assert (new != NULL);
	new->connection = connection_new (host, port);
	if (nick)
		strncpy (new->nick, nick, NICK_LEN - 1);
	new->nick [NICK_LEN - 1] = '\0';
	if (password)
	{
		strncpy (new->password, password, PASS_LEN - 1);
		new->password [PASS_LEN - 1] = '\0';
	}
	else
	{
		new->password [0] = '\0';
	}
	if (channel)
		strncpy (new->channel, channel, CHAN_LEN - 1);
	new->channel [CHAN_LEN - 1] = '\0';
	new->state = IRCState_Disconnected;
	logger_log (logger, LOG_NOTIFY, "IRC: irc_new");
	return new;
}

void irc_destroy (IRC* irc)
{
	if (!irc)
	{
		logger_log (logger, LOG_ALERT, "IRC: irc_destroy: called with null IRC*");
		return;
	}
	if (irc->state != IRCState_Disconnected)
		irc_disconnect (irc);
	if (irc->connection)
		connection_destroy (irc->connection);
	logger_log (logger, LOG_NOTIFY, "IRC: irc_destroy");
	free (irc);
}

IRCState irc_connect (IRC* irc)
{
	//TODO: Handle errors.
	if (connection_connect (irc->connection) != ConnectionState_Connected)
	{
		logger_log (logger, LOG_ALERT, "IRC: irc_connect: connection_connect () returned unexpectedly");
		return IRCState_Disconnected;
	}
	if (irc->state == IRCState_Connected)
	{
		logger_log (logger, LOG_NOTIFY, "IRC: irc_connect: already connected");
		return irc->state;
	}
	if (strlen (irc->password) > 0)
		assert (connection_printf (irc->connection, "PASS %s\r\n", irc->password) > 0);
	assert (connection_printf (irc->connection, "NICK %s\r\n", irc->nick) > 0);
	assert (connection_printf (irc->connection, "USER %s 0 0 :%s Bot\r\n", irc->nick, irc->nick) > 0);
	if (strlen (irc->channel) > 0)
		assert (connection_printf (irc->connection, "JOIN %s\r\n", irc->channel) > 0);
	irc->state = IRCState_Connected;
	logger_log (logger, LOG_NOTIFY, "IRC: irc_connect");
	return IRCState_Connected;
}

void irc_disconnect (IRC* irc)
{
	//TODO: Handle errors.
	assert (connection_printf (irc->connection, "QUIT :Going out of service.\r\n") > 0);
	connection_disconnect (irc->connection);
	logger_log (logger, LOG_NOTIFY, "IRC: irc_disconnect");
	irc->state = IRCState_Disconnected;
}

int irc_send_raw (IRC* irc, char* format, ...)
{
	va_list list;
	int ret;
	va_start (list, format);
	ret =  connection_vprintf (irc->connection, format, list);
	va_end (list);
	logger_log (logger, LOG_NOTIFY, "IRC: irc_send_raw");
	return ret;
}

int irc_send_line (IRC* irc, char* format, ...)
{
	va_list list;
	int ret, len;
	char line [BUFF_LEN];
	len = strlen (format);
	strcpy (line, format);
	line [len] = '\r';
	line [len + 1] = '\n';
	line [len + 2] = '\0';
	va_start (list, format);
	ret = connection_vprintf (irc->connection, line, list);
	va_end (list);
	logger_log (logger, LOG_NOTIFY, "IRC: irc_send_line");
	return ret;
}

int irc_read (IRC* irc, char* buffer, int buffer_len)
{
	int ret;
READ_AGAIN:
	ret = connection_readline (irc->connection, buffer, buffer_len);
	if (ret > 0)
	{
		if (strncmp (buffer, "PING", 4) == 0)
		{
			logger_log (logger, LOG_NOTIFY, "IRC: irc_read: caught %s", buffer);
			buffer [1] = 'O';
			irc_send_raw (irc, "%s\r\n", buffer);
			goto READ_AGAIN;
		}
	}
	logger_log (logger, LOG_NOTIFY, "IRC: irc_read");
	return ret;
}

int irc_read_async (IRC* irc, char* buffer, int buffer_len)
{
	int ret;
READ_AGAIN:
	ret = connection_readline_async (irc->connection, buffer, buffer_len);
	if (ret > 0)
	{
		if (strncmp (buffer, "PING", 4) == 0)
		{
			logger_log (logger, LOG_NOTIFY, "IRC: irc_read_async: caught %s", buffer);
			buffer [1] = 'O';
			irc_send_raw (irc, "%s\r\n", buffer);
			goto READ_AGAIN;
		}
	}
	logger_log (logger, LOG_NOTIFY, "IRC: irc_read_async");
	return ret;
}
