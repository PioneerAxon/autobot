#include <connection_manager.h>

Connection* connection_new (char* host, char* port)
{
	Connection *new = (Connection*) malloc (sizeof (Connection));
	assert (new != NULL);
	new-> connection_fd = -1;
	if (host)
		strncpy (new->host, host, HOST_LEN - 1);
	new->host [HOST_LEN - 1] = '\0';
	if (port)
		strncpy (new->port, port, PORT_LEN - 1);
	new->port [PORT_LEN - 1] = '\0';
	new->state = ConnectionState_Disconnected;
	new->used_buffer_len = 0;
	logger_log (logger, LOG_NOTIFY, "Connection: connection_new: host:%s, port:%s", new->host, new->port);
	return new;
}

void connection_destroy (Connection* connection)
{
	assert (connection);
	if (connection->state != ConnectionState_Disconnected)
	{
		connection_disconnect (connection);
	}
	free (connection);
	logger_log (logger, LOG_NOTIFY, "Connection: connection_destroy");
}

ConnectionState connection_connect (Connection* connection)
{
	struct addrinfo hints, *resolved;
	int error_code;
	if (!connection)
		return ConnectionState_Invalid;
	if (connection->state == ConnectionState_Connected)
		return connection->state;
	memset (&hints, 0, sizeof (struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	//TODO: Handle errors.
	if ((error_code = getaddrinfo (connection->host, connection->port, &hints, &resolved)) != 0)
	{
		logger_log (logger, LOG_CRITICAL, "Connection: connection_connect: getaddrinfo returned %d (%s)", error_code, gai_strerror (error_code));
		return ConnectionState_Invalid;
	}
	connection->connection_fd = socket (resolved->ai_family, resolved->ai_socktype, resolved->ai_protocol);
	if (connection->connection_fd == -1)
	{
		error_code = errno;
		logger_log (logger, LOG_CRITICAL, "Connection: connection_connect: socket returned %d (%s)", error_code, strerror (error_code));
		return ConnectionState_Invalid;
	}
	if (connect (connection->connection_fd, resolved->ai_addr, resolved->ai_addrlen) != 0)
	{
		error_code = errno;
		logger_log (logger, LOG_CRITICAL, "Connection: connection_connect: connect returned %d (%s)", error_code, strerror (error_code));
		return ConnectionState_Invalid;
	}
	free (resolved);
	connection->state = ConnectionState_Connected;
	logger_log (logger, LOG_NOTIFY, "Connection: connection_connect");
	return ConnectionState_Connected;
}

void connection_disconnect (Connection* connection)
{
	if (!connection)
		return;
	//TODO: Handle errors.
	if (connection->state == ConnectionState_Connected)
		close (connection->connection_fd);
	connection->state = ConnectionState_Disconnected;
	logger_log (logger, LOG_NOTIFY, "Connection: connection_disconnect");
}

int connection_printf (Connection* connection, char* format, ...)
{
	va_list list;
	int ret;
	va_start (list, format);
	ret = connection_vprintf (connection, format, list);
	va_end (list);
	return ret;
}

int connection_vprintf (Connection* connection, char* format, va_list list)
{
	char buffer [BUFF_LEN];
	int ret;
	vsnprintf (buffer, BUFF_LEN, format, list);
	if (connection->state != ConnectionState_Connected)
		return 0;
	ret = write (connection->connection_fd, buffer, strlen (buffer));
	logger_log (logger, LOG_NOTIFY, "Connection: connection_vprintf: %d bytes written successfully", ret);
	return ret;
}

///
/// \brief Move content of buffer N bytes
///
/// Move content of buffer by N bytes to the beginning.
///
/// \param[in] connection Connection structure
/// \param[in] n Number of bytes to move
///
static void _connection_buffer_move_n_bytes (Connection* connection, int n)
{
	int l;
	assert (n <= connection->used_buffer_len);
	assert (n > 0);
	for (l = 0; l < connection->used_buffer_len - n; l++)
	{
		connection->buffer [l] = connection->buffer [l + n];
	}
	connection->used_buffer_len -= n;
	connection->buffer [l] = '\0';
}

int connection_read (Connection* connection, char* buffer, int buffer_size)
{
	int ret;
	if (connection->used_buffer_len > 0)
	{
		strncpy (buffer, connection->buffer, connection->used_buffer_len);
		ret = connection->used_buffer_len;
		connection->used_buffer_len = 0;
		return ret;
	}
	ret = read (connection->connection_fd, buffer, buffer_size);
	if (ret < buffer_size - 1)
		buffer [ret] = '\0';
	return ret;
}

int connection_read_async (Connection* connection, char* buffer, int buffer_size)
{
	int ret;
	struct timeval zero;
	fd_set readfds;
	if (connection->used_buffer_len > 0)
	{
		strncpy (buffer, connection->buffer, connection->used_buffer_len);
		ret = connection->used_buffer_len;
		connection->used_buffer_len = 0;
		return ret;
	}
	zero.tv_sec = 0;
	zero.tv_usec = 0;
	FD_ZERO (&readfds);
	FD_SET (connection->connection_fd, &readfds);
	select (connection->connection_fd + 1, &readfds, NULL, NULL, &zero);
	if (FD_ISSET (connection->connection_fd, &readfds))
	{
		ret = read (connection->connection_fd, buffer, buffer_size);
		if (ret < buffer_size - 1)
			buffer [ret] = '\0';
		return ret;
	}
	else
	{
		return -2;
	}
}

int connection_readline (Connection* connection, char* buffer, int buffer_size)
{
	int ret, temp, filled_buffer;
	char* tail;
	tail = NULL;
	filled_buffer = 0;
	if (connection->used_buffer_len > buffer_size)
	{
		strncpy (buffer, connection->buffer, buffer_size);
		_connection_buffer_move_n_bytes (connection, buffer_size);
		return buffer_size;
	}
	if (connection->used_buffer_len > 0)
		tail = strstr (connection->buffer, "\r\n");
	if (tail && (tail - connection->buffer) > buffer_size)
	{
		strncpy (buffer, connection->buffer, buffer_size);
		_connection_buffer_move_n_bytes (connection, buffer_size);
		return buffer_size;
	}
	while (!tail && buffer_size > 0)
	{
		temp = connection->used_buffer_len;
		connection->used_buffer_len = 0;
		ret = connection_read (connection, connection->buffer + temp, BUFF_LEN - temp);
		connection->used_buffer_len = temp;
		if (ret <= 0)
			return ret;
		connection->used_buffer_len += ret;
		if (connection->used_buffer_len > buffer_size)
		{
			strncpy (buffer + filled_buffer, connection->buffer, buffer_size);
			_connection_buffer_move_n_bytes (connection, buffer_size);
			return buffer_size + filled_buffer;
		}
		//Only run strstr when buffer is not full, as it assumes NULL terminated strings.
		if (connection->used_buffer_len != BUFF_LEN)
			tail = strstr (connection->buffer, "\r\n");
		if (connection->used_buffer_len == BUFF_LEN && !tail)
		{
			if (buffer_size < BUFF_LEN)
			{
				strncpy (buffer + filled_buffer, connection->buffer, buffer_size);
				_connection_buffer_move_n_bytes (connection, buffer_size);
				return buffer_size + filled_buffer;
			}
			else
			{
				strncpy (buffer + filled_buffer, connection->buffer, BUFF_LEN);
				buffer_size -= BUFF_LEN;
				filled_buffer += BUFF_LEN;
				connection->used_buffer_len = 0;
			}
		}
	}
	*tail = '\0';
	strcpy (buffer + filled_buffer, connection->buffer);
	_connection_buffer_move_n_bytes (connection, tail - connection->buffer + 2);
	return tail - connection->buffer + filled_buffer;
}

int connection_readline_async (Connection* connection, char* buffer, int buffer_size)
{
	int ret, temp, filled_buffer;
	char* tail;
	tail = NULL;
	filled_buffer = 0;
	if (connection->used_buffer_len > buffer_size)
	{
		strncpy (buffer, connection->buffer, buffer_size);
		_connection_buffer_move_n_bytes (connection, buffer_size);
		return buffer_size;
	}
	if (connection->used_buffer_len > 0)
		tail = strstr (connection->buffer, "\r\n");
	if (tail && (tail - connection->buffer) > buffer_size)
	{
		strncpy (buffer, connection->buffer, buffer_size);
		_connection_buffer_move_n_bytes (connection, buffer_size);
		return buffer_size;
	}
	while (!tail && buffer_size > 0)
	{
		temp = connection->used_buffer_len;
		connection->used_buffer_len = 0;
		ret = connection_read_async (connection, connection->buffer + temp, BUFF_LEN - temp);
		connection->used_buffer_len = temp;
		if (ret <= 0)
			return ret;
		connection->used_buffer_len += ret;
		if (connection->used_buffer_len > buffer_size)
		{
			strncpy (buffer + filled_buffer, connection->buffer, buffer_size);
			_connection_buffer_move_n_bytes (connection, buffer_size);
			return buffer_size + filled_buffer;
		}
		//Only run strstr when buffer is not full, as it assumes NULL terminated strings.
		if (connection->used_buffer_len != BUFF_LEN)
			tail = strstr (connection->buffer, "\r\n");
		if (connection->used_buffer_len == BUFF_LEN && !tail)
		{
			if (buffer_size < BUFF_LEN)
			{
				strncpy (buffer + filled_buffer, connection->buffer, buffer_size);
				_connection_buffer_move_n_bytes (connection, buffer_size);
				return buffer_size + filled_buffer;
			}
			else
			{
				strncpy (buffer + filled_buffer, connection->buffer, BUFF_LEN);
				buffer_size -= BUFF_LEN;
				filled_buffer += BUFF_LEN;
				connection->used_buffer_len = 0;
			}
		}
	}
	*tail = '\0';
	strcpy (buffer + filled_buffer, connection->buffer);
	_connection_buffer_move_n_bytes (connection, tail - connection->buffer + 2);
	return tail - connection->buffer + filled_buffer;
}
