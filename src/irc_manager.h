#ifndef __IRC_MANAGER__
#define __IRC_MANAGER__

#include <stdlib.h>
#include <assert.h>
#include <errno.h>

#include <connection_manager.h>
#include <logger.h>
#include <constants.h>

extern Logger* logger;

///
/// \brief IRC connection state
///
typedef enum _IRCState
{
	IRCState_Invalid,		///< Invalid IRC connection state
	IRCState_Connected,		///< IRC server connected
	IRCState_Disconnected,		///< IRC server disconnected
} IRCState;

///
/// \brief IRC structure
///
typedef struct _IRC
{
	Connection* connection;		///< Connection instance
	char nick [NICK_LEN];		///< IRC Nickname
	char password [PASS_LEN];	///< IRC Password
	char channel [CHAN_LEN];	///< IRC Channel
	IRCState state;			///< IRCState of IRC connection
} IRC;


///
/// \brief IRC constructur
///
/// Create new IRC instance.
///
/// \param[in] host Hostname or IRC server
/// \param[in] port Port number of IRC server
/// \param[in] nick Nickname to be used to register
/// \param[in] password Password to use for Nickname (can be NULL)
/// \param[in] channel Channel to connect to
///
/// \return Newly constructed IRC instance
///
IRC* irc_new (char* host, char* port, char* nick, char* password, char* channel);

///
/// \brief IRC destructor
///
/// Disconnect and destroy an IRC instance
///
/// \param[in] irc IRC structure
///
void irc_destroy (IRC* irc);

///
/// \brief IRC connect to server
///
/// Connects an IRC instance to server.
///
/// \param[in] irc IRC structure
///
/// \return IRCState of connection
///
IRCState irc_connect (IRC* irc);

///
/// \brief IRC disconnect connection
///
/// Disconnects a connected IRC instance.
///
/// \param[in] irc IRC structure
///
void irc_disconnect (IRC* irc);

///
/// \brief Send raw bytes to IRC
///
/// Send raw bytes to IRC stream.
///
/// \param[in] irc IRC structure
/// \param[in] format Print format
/// \param[in] ... Argument list
///
/// \return Number of bytes written
///
int irc_send_raw (IRC* irc, char* format, ...);

///
/// \brief Send line to IRC
///
/// Send raw bytes with extra termination symbol "\r\n" to IRC stream.
///
/// \param[in] irc IRC structure
/// \param[in] format Print format
/// \param[in] ... Argument list
///
/// \return Number of bytes written
///
int irc_send_line (IRC* irc, char* format, ...);

///
/// \brief Read from IRC connection
///
/// Read bytes into buffer from IRC stream in a blocking manner.
///
/// \param[in] irc IRC structure
/// \param[out] buffer Buffer to write into
/// \param[in] buffer_len Length of buffer
///
/// \return Number of bytes read
///
int irc_read (IRC* irc, char* buffer, int buffer_len);

///
/// \brief Read from IRC connection Asynchronously
///
/// Read bytes into buffer from IRC stream in a non-blocking manner.
///
/// \param[in] irc IRC state
/// \param[out] buffer Buffer to write into
/// \param[in] buffer_len Length of buffer
///
/// \return Number of bytes read.
///         -2 if nothing to read.
///
int irc_read_async (IRC* irc, char* buffer, int buffer_len);

#endif
