#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <assert.h>

#include <constants.h>

///
/// \brief Logging level of Logger
///
typedef enum _LogLevel
{
	LOG_NONE,			///< Log nothing
	LOG_CRITICAL,			///< Log only critical / Critical message
	LOG_ALERT,			///< Log alerts and above / Alert message
	LOG_NOTIFY,			///< Log notify and above / Notification message
	LOG_ALL				///< Log everything
} LogLevel;

///
/// \brief Logger structure
///
typedef struct _Logger
{
	FILE* file;			///< FILE pointer to the log file
	LogLevel max_log_level;		///< Maximum LogLevel for given instance
} Logger;

///
/// \brief Convert LogLevel to string
///
/// \param[in] level LogLevel to convert
///
/// \return String with LogLevel
///
const char* log_level_to_string (LogLevel level);

///
/// \brief Logger constructor
///
/// Create new instance of Logger.
///
/// \param[in] file_name Name of the logger file
/// \param[in] max_log_level LogLevel upto which to write into log file
///
/// \return Newly constructed Logger instance
///
Logger* logger_new (const char* file_name, LogLevel max_log_level);

///
/// \brief Log a line into Log file
///
/// Logs a message into Logger if the LogLevel permits.
///
/// \param[in] logger Logger structure
/// \param[in] level LogLevel of the message
/// \param[in] message Message format
/// \param[in] ... Argument list
///
void logger_log (Logger* logger, LogLevel level, char* message, ...);

///
/// \brief Logger destructor
///
/// Destroy Logger instance.
///
/// \param[in] logger Logger structure
///
void logger_destroy (Logger* logger);

#endif
