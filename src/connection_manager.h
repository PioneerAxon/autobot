#ifndef __CONNECTION_MANAGER__
#define __CONNECTION_MANAGER__


#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <errno.h>

#include <logger.h>
#include <constants.h>

extern Logger* logger;

///
/// \brief Connection state.
///
typedef enum _ConnectionState
{
	ConnectionState_Invalid,		///< Connection in invalid state
	ConnectionState_Disconnected,		///< Connection in disconnected state
	ConnectionState_Connected,		///< Connection in connected state
} ConnectionState;

///
/// \brief Connection structure
///
typedef struct _Connection
{
	int connection_fd;			///< Connection file descriptor
	char host [HOST_LEN];			///< Hostname of the server
	char port [PORT_LEN];			///< Port number string of the server
	char buffer [BUFF_LEN];			///< Buffer to read the data into
	int used_buffer_len;			///< Bytes of buffer used
	ConnectionState state;			///< ConnectionState of Connection
} Connection;

///
/// \brief Connection constructor
///
/// Constructor of Connection structure.
///
/// \param[in] host Hostname of the server
/// \param[in] port Port number of th server
///
/// \return Newly created Connection structure
///
Connection* connection_new (char* host, char* port);

///
/// \brief Connection destructor
///
/// Destroy the Connection structure.
///
/// \param[in] connection Connection structure
///
void connection_destroy (Connection* connection);

///
/// \brief Connect the Connection
///
/// Connect the Connection to the given Host and Port of the server.
///
/// \param[in] connection Connection structure
///
/// \return ConnectionState after connect attempt
///
ConnectionState connection_connect (Connection* connection);

///
/// \brief Disconnect the Connection
///
/// Disconnect the Connection from given Host and Port.
///
/// \param[in] connection Connection structure
///
void connection_disconnect (Connection* connection);

///
/// \brief Print into Connection
///
/// Print the content into the Connection stream pointed by connection_fd.
///
/// \param[in] connection Connection structure
/// \param[in] format Formatting string
/// \param[in] ... Content
///
/// \return Number of bytes written
///
int connection_printf (Connection* connection, char* format, ...);

///
/// \brief vprintf into Connection
///
/// Print the content into the Connection stream pointed by connection_fd.
///
/// \param[in] connection Connection structure
/// \param[in] format Formatting string
/// \param[in] list va_list of content
///
/// \return Number of bytes written
///
int connection_vprintf (Connection* connection, char* format, va_list list);

///
/// \brief Read from Connection
///
/// Reads incoming message from Connection in blocking manner.
///
/// \param[in] connection Connection structure
/// \param[out] buffer Buffer to write into
/// \param[in] buffer_size Length of buffer
///
/// \return Number of bytes read
///
int connection_read (Connection* connection, char* buffer, int buffer_size);

///
/// \brief Read from Connection asynchronously
///
/// Reads incoming message from Connection in non-blocking manner.
///
/// \param[in] connection Connection structure
/// \param[out] buffer Buffer to write into
/// \param[in] buffer_size Length of buffer
///
/// \return Number of bytes read.
///         -2 on no pending message to read.
///
int connection_read_async (Connection* connection, char* buffer, int buffer_size);

///
/// \brief Read one line from Connection
///
/// Reads one line of incoming message from Connection in blocking manner.
///
/// \param[in] connection Connection structure
/// \param[out] buffer Buffer to write into
/// \param[in] buffer_size Length of the buffer
///
/// \return Number of bytes read
///
int connection_readline (Connection* connection, char* buffer, int buffer_size);

///
/// \brief Read one line from Connection asynchronously
///
/// Reads one line of incoming message from Connection in non-blocking manner.
///
/// \param[in] connection Connection structure
/// \param[out] buffer Buffer to write into
/// \param[in] buffer_size Length of buffer
///
/// \return Number of bytes read.
///         -2 on no pending line to read.
///
int connection_readline_async (Connection* connection, char* buffer, int buffer_size);

#endif
