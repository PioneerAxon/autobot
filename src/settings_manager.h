#ifndef __SETTINGS_MANAGER__
#define __SETTINGS_MANAGER__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <errno.h>

#include <logger.h>
#include <constants.h>

extern Logger* logger;

///
/// \brief Setting with single attribute and value
///
typedef struct _Setting
{
	char attribute [ATTR_LEN];	///< Attribute of setting
	char value [VAL_LEN];		///< Value of attribute
} Setting;

///
/// \brief Collection of Setting instances
///
typedef struct _Settings
{
	int settings_count;		///< Number of Setting saved in Settings instance
	Setting* settings;		///< Array containing Setting
} Settings;

///
/// \brief Import Settings from SETTINGS_FILE_NAME file
///
/// \return Newly created Settings instance
///
Settings* settings_import ();

///
/// \brief Get value of given attribute
///
/// Return Value of specific attribute stored in Settings.
///
/// \param[in] settings Settings structure
/// \param[in] attribute Attribute to find
///
/// \return Value of attribute, if found.
///         SPLIT_STR, if not found.
///
char* settings_get_value (Settings* settings, const char* attribute);

///
/// \brief Settings destructor
///
/// Destroy Settings instance.
///
/// \param[in] settings Settings structure
///
void settings_destroy (Settings* settings);

#endif
