#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

#include <bot_manager.h>
#include <connection_manager.h>
#include <constants.h>
#include <irc_manager.h>
#include <logger.h>
#include <settings_manager.h>
#include <acl_manager.h>

Logger* logger;	///< Global Logger instance.
Bot* bot;	///< Global Bot instance.
ACL* acl;	///< Global ACL instance

///
/// \brief Callback on quit command
///
void* quit_cb (Bot* bot, const char* full_message, const char* from, const char* to, const char* content)
{
	bot_quit_main_loop (bot);
	return NULL;
}

///
/// \brief Callback on echo request
///
void* echo_cb (Bot* bot, const char* full_message, const char* from, const char* to, const char* content)
{
	irc_send_line (bot->irc, "PRIVMSG %s :%s", from, content + 5);
	return NULL;
}

///
/// \brief Callback on unknown request
///
void* display_cb (Bot* bot, const char* full_message, const char* from, const char* to, const char* content)
{
	printf ("From: %s, To: %s, Content: %s\n", from, to, content);
	return NULL;
}

///
/// \brief Callback on get request
///
void* get_cb (Bot* bot, const char* full_message, const char* from, const char* to, const char* content)
{
	if (!acl_check_access (acl, from))
	{
		//TODO: Log event.
		printf ("Get request (%s) from unknown source : %s\n", content, from);
		return NULL;
	}
	//TODO: Write get_value logic here.
	printf ("Get request (%s) from authorized source : %s\n", content, from);
	return NULL;
}

///
/// \brief Callback on set request
///
void* set_cb (Bot* bot, const char* full_message, const char* from, const char* to, const char* content)
{
	if (!acl_check_access (acl, from))
	{
		//TODO: Log event.
		printf ("Set request (%s) from unknown source : %s\n", content, from);
		return NULL;
	}
	printf ("Set request (%s) from authorized source : %s\n", content, from);
	return NULL;
}

///
/// \brief Initialize global variables
///
/// Initializes global ACL, Bot & Logger structures.
///
void init_autobot ()
{
	char nick [NICK_LEN];
	char password [PASS_LEN];
	char channel [CHAN_LEN];
	char host [HOST_LEN];
	char port [PORT_LEN];

	Settings* settings;
	logger = logger_new (LOG_FILE_NAME, DEFAULT_LOG_LEVEL);
	settings = settings_import ();

	acl = acl_new (settings);

	strcpy (nick, settings_get_value (settings, "IRC.NICK"));
	assert (strlen (nick) > 0 && nick [0] != SPLIT_CHAR);

	strcpy (password, settings_get_value (settings, "IRC.PASS"));
	if (password [0] != SPLIT_CHAR)
		strcpy (password, "");

	strcpy (channel, settings_get_value (settings, "IRC.CHAN"));
	if (channel [0] == SPLIT_CHAR)
		strcpy (channel, "");

	strcpy (host, settings_get_value (settings, "IRC.HOST"));
	if (strlen (host) == 0 || host [0] == SPLIT_CHAR)
		strcpy (host, "");

	strcpy (port, settings_get_value (settings, "IRC.PORT"));
	if (strlen (port) == 0 || port [0] == SPLIT_CHAR)
		strcpy (port, "");

	bot = bot_new (host, port, nick, password, channel);
	bot_insert_callback (bot, "exit", quit_cb, CallbackType_CaseInsensitive);
	bot_insert_callback (bot, "quit", quit_cb, CallbackType_CaseInsensitive);
	bot_insert_callback (bot, "bye", quit_cb, CallbackType_CaseInsensitive);
	bot_insert_callback (bot, "echo", echo_cb, CallbackType_CaseInsensitive);
	bot_insert_callback (bot, "get", get_cb, CallbackType_CaseInsensitive);
	bot_insert_callback (bot, "set", set_cb, CallbackType_CaseInsensitive);
	bot_insert_default_callback (bot, display_cb);
	settings_destroy (settings);
}

///
/// \brief Destroys global variables
///
/// Destroys global instances of ACL, Bot & Logger structures.
///
void destroy_autobot ()
{
	acl_destroy (acl);
	bot_destroy (bot);
	logger_destroy (logger);
}

int main ()
{
	init_autobot ();
	printf ("Entering main loop.\n");
	bot_main_loop (bot);
	destroy_autobot ();
	return EXIT_SUCCESS;
}
