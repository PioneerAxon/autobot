#ifndef __MESSAGE_HANDLER_H__
#define __MESSAGE_HANDLER_H__

#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <bot_manager.h>
#include <irc_manager.h>
#include <connection_manager.h>
#include <logger.h>
#include <settings_manager.h>

extern Logger* logger;

///
/// \brief Type of message parsed
///
typedef enum _MessageType
{
	MessageType_Unknown,	///< Unknown MessageType
	MessageType_Notice,	///< NOTICE message
	MessageType_Mode,	///< MODE change message
	MessageType_Numeric,	///< Numeric response message
} MessageType;

///
/// \struct Bot
///
struct _Bot;

///
/// \brief Try parse PRIVMSG type of message
///
/// Try to parse a message of type PRIVMSG.
///
/// \param[in] message Received message
/// \param[out] from From whom the message is received
/// \param[out] to For whom the message is intended
/// \param[out] content Message content
///
/// \return Zero to indicate failure.
///         Non-zero to indicate success.
///
int message_try_parse_PRIVMSG (const char* message, char* from, char* to, char* content);

///
/// \brief Try to parse message
///
/// Try to break down message in basic messaging format ":name command <parameter list>".
///
/// \param[in] message Received message
/// \param[out] name First argument of basic messaging format (generally Hostname or Nickname)
/// \param[out] command Second argument of basic messaging format (indicates type of message)
/// \param[out] list Remaining part of basic messaging format (parameter list)
///
/// \return Zero to indicate failure.
///         Non-zero to indicate success.
///
int message_try_parse_message (const char* message, char* name, char* command, char* list);

///
/// \brief Consume message
///
/// Consume message and response to specific types of messages from server.
///
/// \param[in] bot Bot structure
/// \param[in] message Received message
///
/// \return MessageType of message
///
MessageType message_consume (struct _Bot* bot, const char* message);

#endif
