#ifndef __BOT_MANAGER_H__
#define __BOT_MANAGER_H__

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <irc_manager.h>
#include <logger.h>
#include <constants.h>
#include <message_handler.h>

extern Logger* logger;

typedef struct _Callback Callback;

///
/// \brief Type of Callback detection
///
typedef enum _CallbackType
{
	CallbackType_CaseSensitive,	///< Case sensitive scanning of callback
	CallbackType_CaseInsensitive,	///< Case insensitive scanning of callback
} CallbackType;

///
/// \brief IRC bot structure
///
typedef struct _Bot
{
	IRC* irc;			///< IRC instance
	Callback** callbacks;		///< Callback array
	int callback_count;		///< Length of Callback array
	int allow_in_main_loop;		///< Whether to continue main loop or not
	void* (*default_callback) (struct _Bot* bot, const char* full_message, const char* from, const char* to, const char* content);		///< Default Callback, called when no other callback is matched
} Bot;

///
/// \brief Bot constructor
///
/// Construct Bot from different parameters.
///
/// \param[in] host Hostname of IRC server
/// \param[in] port Port string of IRC server
/// \param[in] nick Nickname on IRC network
/// \param[in] password Password (if registered) on IRC network
/// \param[in] channel Channel to join on IRC network (if any)
///
/// \return The newly constructed Bot structure.
///
Bot* bot_new (char* host, char* port, char* nick, char* password, char* channel);

///
/// \brief Insert Callback in Bot
///
/// Insert Callback on given message sequence into the Bot structure.
///
/// \param[in] bot Bot structure
/// \param[in] on_message Message on which Callback should be called
/// \param[in] function Callback function
/// \param[in] type CallbackType of Callback
///
void bot_insert_callback (Bot* bot, char* on_message, void* (*function) (Bot* bot, const char* full_message, const char* from, const char* to, const char* content), CallbackType type);

///
/// \brief Insert default Callback in Bot
///
/// Insert default Callback on given message sequence into the bot structure.
/// \warning It's called only when no other callbacks are present to handle the request.
///
/// \param[in] bot Bot structure
/// \param[in] function Callback function
///
void bot_insert_default_callback (Bot* bot, void* (*function) (Bot* bot, const char* full_message, const char* from, const char* to, const char* content));

///
/// \brief Do background Bot events
///
/// Do Bot events in background.
/// \warning It must be called from a function periodically if the function is expected to take long to return.
///
/// \param [in] bot Bot structure
///
void bot_do_events (Bot* bot);

///
/// \brief Enter main loop
///
/// Do Bot events in a loop.
///
/// \param[in] bot Bot structure
void bot_main_loop (Bot* bot);

///
/// \brief Break main loop
/// Sets a flag to break main loop in next iteration.
/// \warning This method is not thread-safe.
///
/// \param[in] bot Bot structure
///
void bot_quit_main_loop (Bot* bot);

///
/// \brief Bot destructor
///
/// Destroy Bot structure.
///
/// \param[in] bot Bot structure
void bot_destroy (Bot* bot);

#endif
