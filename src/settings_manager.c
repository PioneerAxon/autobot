#include <settings_manager.h>

///
/// \brief Split Setting
///
/// Split input into attribute and value
///
/// \param[in] input Input string
/// \param[out] attribute Attribute name
/// \param[out] value Value for the attribute
///
/// \return Non-zero on success.
///         Zero on failure.
///
static int setting_split (char* input, char* attribute, char* value)
{
	char* split_point;
	int len;
	//TODO: Handle errors.
	if (!input || !attribute || !value)
	{
		logger_log (logger, LOG_ALERT, "Settings: setting_split: called with null pointer");
		return 0;
	}

	while ((*input) == ' ')
		input++;
	split_point = strchr (input, SPLIT_CHAR);
	if (!split_point)
	{
		logger_log (logger, LOG_ALERT, "Settings: setting_split: split point \'%c\' not found", SPLIT_CHAR);
		return 0;
	}
	len = split_point - input;
	strncpy (attribute, input, len);
	while (attribute [len - 1] == ' ')
		len--;
	attribute [len] = '\0';

	split_point++;
	while ((*split_point) == ' ')
		split_point++;
	len = strlen (split_point);
	strncpy (value, split_point, len);
	while (value [len - 1] == ' ')
		len--;
	value [len] = '\0';

	logger_log (logger, LOG_NOTIFY, "Settings: setting_split");
	return 1;
}

///
/// \brief Insert Setting into Settings.
///
/// Insert given Setting into Settings structure.
///
/// \param[in] settings Settings structure
/// \param[in] attribute Name of attribute
/// \param[in] value Value for the attribute
///
static void settings_insert_setting (Settings* settings, char* attribute, char* value)
{
	if (!settings)
	{
		logger_log (logger, LOG_ALERT, "Settings: settings_insert_setting: called with null pointer");
	}
	settings->settings = (Setting*) realloc (settings->settings, sizeof (Setting) * (settings->settings_count + 1));
	//TODO: Handle errors.
	assert (settings->settings);
	strncpy (settings->settings [settings->settings_count].attribute, attribute, ATTR_LEN);
	strncpy (settings->settings [settings->settings_count].value, value, VAL_LEN);
	settings->settings_count++;
	logger_log (logger, LOG_NOTIFY, "Settings: settings_insert_setting");
}

Settings* settings_import ()
{
	int file;
	int len;
	char buffer [1280];
	char attribute [ATTR_LEN];
	char value [VAL_LEN];
	char ch;
	Settings* new;
	new = (Settings*) malloc (sizeof (Settings));
	//TODO: Handle errors.
	assert (new);
	new->settings = NULL;
	new->settings_count = 0;
	file = open (SETTINGS_FILE_NAME, O_RDONLY);
	if (file == -1)
	{
		logger_log (logger, LOG_CRITICAL, "Settings: settings_import: unable to open file %s", SETTINGS_FILE_NAME);
		return new;
	}
	while (1)
	{
		len = 0;
		while (1)
		{
			if (read (file, &ch, 1) == 0)
			{
				len = -1;
				break;
			}
			buffer [len] = ch;
			if (ch == '\n' || ch == '\r')
				break;
			len++;

		}
		if (len < 0)
			break;
		if (len < 1)
			continue;
		if (buffer [0] == '#')
			continue;
		buffer [len] = '\0';
		if (setting_split (buffer, attribute, value))
		{
			settings_insert_setting (new, attribute, value);
		}
	}
	close (file);
	logger_log (logger, LOG_NOTIFY, "Settings: settings_import");
	return (new);
}

char* settings_get_value (Settings* settings, const char* attribute)
{
	int l;
	int len = strlen (attribute);
	if (!len)
	{
		logger_log (logger, LOG_NOTIFY, "Settings: settings_get_value");
		return SPLIT_STR;
	}
	for (l = 0; l < settings->settings_count; l++)
	{
		if (strcmp (settings->settings [l].attribute, attribute) == 0)
		{
			logger_log (logger, LOG_NOTIFY, "Settings: setting_get_value");
			return settings->settings [l].value;
		}
	}
	logger_log (logger, LOG_NOTIFY, "Settings: setting_get_value");
	return SPLIT_STR;
}

void settings_destroy (Settings* settings)
{
	if (!settings)
	{
		logger_log (logger, LOG_ALERT, "Settings: settings_destroy: called with null pointer");
		return;
	}
	if (settings->settings_count > 0)
		free (settings->settings);
	logger_log (logger, LOG_NOTIFY, "Settings: setting_destroy");
	free (settings);
}
