#include <message_handler.h>

///
/// \brief cleanup nickname
///
/// Sets every non-recognized characters from given Nickname to NULL.
///
/// \param[in,out] nick Nickname to cleanup
///
static void _nick_cleanup (char* nick)
{
	int l, len;
	len = strlen (nick);
	for (l = 0; l < len; l++)
	{
		if (strchr ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-[]\\^{}|`", nick [l]) == NULL)
			nick [l] = '\0';
	}
}

///
/// \brief Check if integer
///
/// Checks whether string contains an integer value or not.
///
/// \param[in] string Input string to check
///
/// \return Zero on non-integer string.
///         Non-zero on integer.
///
static int _is_int (char* string)
{
	int l;
	l = strlen (string);
	while (l--)
	{
		if (strchr ("0123456789", string [l]) == NULL)
			return 0;
	}
	return 1;
}

int message_try_parse_PRIVMSG (const char* message, char* from, char* to, char* content)
{
	const char* head;
	const char* tail;
	int len;
	if (!message || !from || !to || !content)
		return 0;
	if (*message != ':')
		return 0;

	head = message + 1;
	tail = strchr (head, ' ');
	if (!tail)
		return 0;
	len = tail - head;
	strncpy (from, head, len);
	from [len] = '\0';
	_nick_cleanup (from);

	head = tail;
	while (*head == ' ')
		head++;
	tail = strchr (head, ' ');
	if (!tail)
		return 0;
	if (strncmp (head, "PRIVMSG", 7) != 0)
		return 0;

	head = tail;
	while (*head == ' ')
		head++;
	tail = strchr (head, ' ');
	if (!tail)
		return 0;
	len = tail - head;
	strncpy (to, head, len);
	to [len] = '\0';
	_nick_cleanup (to);

	head = tail;
	while (*head == ' ')
		head++;
	if (*head != ':')
		return 0;
	head++;
	len = strlen (head);
	strncpy (content, head, len);
	content [len--] = '\0';
	while (content [len] == ' ' || content [len] == '\r' || content [len] == '\n')
		content [len--] = '\0';
	return 1;
}

int message_try_parse_message (const char* message, char* name, char* command, char* list)
{
	const char* head;
	const char* tail;
	int len;
	if (!message || !name || !command || !list)
		return 0;
	if (*message != ':')
		return 0;

	head = message + 1;
	tail = strchr (head, ' ');
	if (!tail)
		return 0;
	len = tail - head;
	strncpy (name, head, len);
	name [len] = '\0';

	head = tail;
	while (*head == ' ')
		head++;
	tail = strchr (head, ' ');
	if (!tail)
		return 0;
	len = tail - head;
	strncpy (command, head, len);
	command [len] = '\0';

	head = tail;
	while (*head == ' ')
		head++;
	len = strlen (message);
	strncpy (list, head, len);
	list [len--] = '\0';
	while (list [len] == ' ' || list [len] == '\r' || list [len] == '\n')
		list [len--] = '\0';
	return 1;
}

MessageType message_consume (struct _Bot* bot, const char* message)
{
	char name [HOST_LEN];
	char command [BUFF_LEN];
	char list [BUFF_LEN];
	int response;
	if (!message_try_parse_message (message, name, command, list))
	{
		logger_log (logger, LOG_ALERT, "Message: message_consume: discarding unknown message %s\n", message);
		return MessageType_Unknown;
	}
	if (strncmp (command, "MODE", 4) == 0)
	{
		_nick_cleanup (name);
		if (strcmp (name, bot->irc->nick) == 0)
		{
			//TODO: MODE changed for us!!
			return MessageType_Mode;
		}
		logger_log (logger, LOG_ALERT, "Message: message_consume: discarding MODE message not intended for us: %s", message);
		return MessageType_Mode;
	}
	if (strncmp (command, "NOTICE", 6) == 0)
	{
		logger_log (logger, LOG_NOTIFY, "Message: message_consume: NOTICE: %s", message);
		return MessageType_Notice;
	}
	if (_is_int (command))
	{
		response = atoi (command);
		switch (response)
		{
			default:
				logger_log (logger, LOG_NOTIFY, "Message: message_consume: Unknown command %d: %s", response, message);
		}
		return MessageType_Numeric;
	}
	return MessageType_Unknown;
}
