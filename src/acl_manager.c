#include <acl_manager.h>

///
/// \brief Insert valuse in ACL
///
/// Insert value in ACL structure.
///
/// \param[in,out] acl ACL structure
/// \param[in] start Pointer to the start point of string
/// \param[in] end Pointer to the end point of string
///
static void _acl_insert_value (ACL* acl, const char* start, const char* end)
{
	int len = 0;
	len = end - start;
	acl->list = (char**) realloc (acl->list, sizeof (char*) * (acl->list_len + 1));
	acl->list [acl->list_len] = (char*) malloc (sizeof (char) * (len + 1));
	strncpy (acl->list [acl->list_len], start, len);
	acl->list [acl->list_len] [len] = '\0';
	acl->list_len++;
}

///
/// \brief Split & insert values to ACL
///
/// Splits the values from a list, and inserts them into ACL structure.
///
/// \param[in,out] acl ACL structure
/// \param[in] value Space saperated list of values
///
static void _acl_split_insert_acl (ACL* acl, const char* value)
{
	const char* start;
	const char* end;
	start = end = value;
	while (1)
	{
		while (*end == ' ')
			end++;
		start = end;
		end = start;
		while (*end != ' ' && *end != '\r' && *end != '\n' && *end != '\0')
			end++;
		_acl_insert_value (acl, start, end);
		if (*end == '\r' || *end == '\n' || *end == '\0')
			break;
	}
}

ACL* acl_new (Settings* settings)
{
	char value [BUFF_LEN];
	ACL* new;
	if (!settings)
		return NULL;
	new = (ACL*) malloc (sizeof (ACL));
	assert (new != NULL);

	new->list_len = 0;
	new->list = NULL;

	strcpy (value, settings_get_value (settings, "ACL.ROOT"));
	if (strncmp (value, SPLIT_STR, strlen (SPLIT_STR)) == 0 || strlen (value) == 0)
	{
		return new;
	}
	_acl_split_insert_acl (new, value);
	return new;
}

int acl_check_access (ACL* acl, const char* name)
{
	int l;
	for (l = 0; l < acl->list_len; l++)
	{
		if (strcmp (name, acl->list [l]) == 0)
			return 1;
	}
	return 0;
}

void acl_destroy (ACL* acl)
{
	if (!acl)
		return;
	while (acl->list_len--)
	{
		free (acl->list [acl->list_len]);
	}
	if (acl->list)
		free (acl->list);
	free (acl);
}
